#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>

class NonCopyable
{
public:
  NonCopyable() = default;
  NonCopyable(const NonCopyable&) = delete;
  NonCopyable& operator=(const NonCopyable&) = delete;
  virtual ~NonCopyable() = 0;
};

class Logger : NonCopyable
{
public:
  static Logger& get_instance();

  Logger(const Logger&) = delete;

  Logger& operator=(const Logger&) = delete;
  ~Logger() override;

  void log(const std::string& msg);

private:
  Logger();

  void dump_log_msg();

  std::thread io_worker_;
  std::mutex mutex_;
  std::condition_variable cond_var_;
  std::queue<std::string> msg_queue_;

  bool finish = false;
};

Logger::Logger() {
  io_worker_ = std::thread {&Logger::dump_log_msg, this};
}

Logger::~Logger() {
  {
    std::lock_guard lock(mutex_);
    finish = true;
    cond_var_.notify_all();
  }

  if (io_worker_.joinable()) io_worker_.join();
}

void Logger::log(const std::string &msg) {
  std::lock_guard lock(mutex_);

  msg_queue_.push(msg);
  cond_var_.notify_all();
}

void Logger::dump_log_msg() {
  std::ofstream error_log("error_log.txt");

  if (error_log.fail()) {
    std::cerr << "Can not open error log file" << std::endl;
  }

  std::unique_lock lock(mutex_);
  for (;;) {
    if (!finish) cond_var_.wait(lock);

    lock.unlock();
    for (;;) {
      lock.lock();
      if (msg_queue_.empty()) {
        break;
      } else {
        error_log << msg_queue_.front() << std::endl;
        msg_queue_.pop();
      }
      lock.unlock();
    }

    if (finish) {
      break;
    }
  }
}
Logger &Logger::get_instance() {
  static Logger logger;
  return logger;
}

int main()
{
  std::vector<std::thread> threads;
  threads.reserve(42);
  for (int id = 0; id < 42; ++id) {
    threads.emplace_back([](int id, Logger& logger)
                         {
                           std::stringstream ss;
                           ss << "Log msg from thread " << id;
                           logger.log(ss.str());
                         }, id, std::ref(Logger::get_instance()));
  }

  for (auto& t : threads) {
    t.join();
  }

  return 0;
}
